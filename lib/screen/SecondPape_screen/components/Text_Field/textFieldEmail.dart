import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:sing_in_test/utilities/constans.dart';

class TextFieldSeEmail extends StatefulWidget {
  TextFieldSeEmail({Key key}) : super(key: key);

  @override
  _TextFieldSeEmailState createState() => _TextFieldSeEmailState();
}

class _TextFieldSeEmailState extends State<TextFieldSeEmail> {
  @override
  Widget build(BuildContext context) {
    return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Text(
        'Email',
        style: kLabelStyle,
      ),
      SizedBox(height: 10.0),
      Container(
        alignment: Alignment.centerRight,
        decoration: kBoxDecorationSecondStyle,
        height: 60.0,
        child: TextField(
          keyboardType: TextInputType.emailAddress,
          style: GoogleFonts.openSans(
            color: Colors.white,
          ),
          decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14.0),
              prefixIcon: Icon(
                Icons.email,
                color: Colors.white,
              ),
              hintText: 'Enter your Email',
              hintStyle: kHintTextStyle),
        ),
      )
    ],
  );
  }
}