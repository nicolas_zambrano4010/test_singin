import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:sing_in_test/utilities/constans.dart';

class TextFieldPAss extends StatefulWidget {
  TextFieldPAss({Key key}) : super(key: key);

  @override
  _TextFieldPAssState createState() => _TextFieldPAssState();
}

class _TextFieldPAssState extends State<TextFieldPAss> {
  @override
  Widget build(BuildContext context) {
    return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Text(
        'Password',
        style: kLabelStyle,
      ),
      SizedBox(height: 10.0),
      Container(
        alignment: Alignment.centerRight,
        decoration: kBoxDecorationSecondStyle,
        height: 60.0,
        child: TextField(
          obscureText: true,
          style: GoogleFonts.openSans(
            color: Colors.white,
          ),
          decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14.0),
              prefixIcon: Icon(
                Icons.lock,
                color: Colors.white,
              ),
              hintText: 'Enter your Passsword',
              hintStyle: kHintTextStyle),
        ),
      )
    ],
  );
  }
}