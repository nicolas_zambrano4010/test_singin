import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:sing_in_test/utilities/constans.dart';

class TextFieldName extends StatefulWidget {
  TextFieldName({Key key}) : super(key: key);

  @override
  _TextFieldNameState createState() => _TextFieldNameState();
}

class _TextFieldNameState extends State<TextFieldName> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 60.0),
      child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Name or NickName',
          style: kLabelStyle,
        ),
        SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerRight,
          decoration: kBoxDecorationSecondStyle,
          height: 60.0,
          child: TextField(
            keyboardType: TextInputType.name,
            style: GoogleFonts.openSans(
              color: Colors.white,
            ),
            decoration: InputDecoration(
                border: InputBorder.none,
                contentPadding: EdgeInsets.only(top: 14.0),
                prefixIcon: Icon(
                  Icons.people,
                  color: Colors.white,
                ),
                hintText: 'Enter your Name',
                hintStyle: kHintTextStyle),
          ),
        )
      ],
  ),
    );
  }
}