import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class ButtonSignUp extends StatefulWidget {
  ButtonSignUp({Key key}) : super(key: key);

  @override
  _ButtonSignUpState createState() => _ButtonSignUpState();
}

class _ButtonSignUpState extends State<ButtonSignUp> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 60.0),
      width: double.infinity,
      child: RaisedButton(
        elevation: 20,
        onPressed: () => print('Login Button Pressed'),
        padding: EdgeInsets.all(15.0),
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
        color: Colors.white,
        child: Text(
          'SIGN UP',
          style: GoogleFonts.openSans(
            color: Colors.cyan[500],
            letterSpacing: 1.5,
            fontSize: 18.0,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}