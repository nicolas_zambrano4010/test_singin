import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:sing_in_test/screen/SecondPape_screen/components/Text_Field/textFiel_Name.dart';
import 'package:sing_in_test/screen/SecondPape_screen/components/Text_Field/textFieldEmail.dart';
import 'package:sing_in_test/screen/SecondPape_screen/components/Text_Field/text_field_pass.dart';
import 'package:sing_in_test/screen/SecondPape_screen/components/button/buttonSign_Up.dart';
import 'package:sing_in_test/utilities/constans.dart';

class SecondPage extends StatefulWidget {
  SecondPage({Key key}) : super(key: key);

  @override
  _SecondPageState createState() => _SecondPageState();
}

List<Color> colorList = [
  Colors.cyan[200],
  Colors.cyan[300],
  Colors.cyan[400],
  Colors.cyan
];

class _SecondPageState extends State<SecondPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.black,
      ),
      body: Stack(
        children: [
          Container(
            height: double.infinity,
            width: double.infinity,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: colorList,
                stops: [0.1, 0.4, 0.7, 0.9],
              ),
            ),
          ),
          Container(
            height: 150.0,
            width: double.infinity,
            decoration: BoxDecoration(
              color: Colors.black,
              boxShadow: [BoxShadow(blurRadius: 40.0)],
              borderRadius: BorderRadius.vertical(
                bottom: Radius.elliptical(
                  MediaQuery.of(context).size.width,
                  100.0,
                ),
              ),
            ),
          ),
          Container(
              height: double.infinity,
              child: SingleChildScrollView(
                physics: AlwaysScrollableScrollPhysics(),
                padding: EdgeInsets.symmetric(horizontal: 25.0, vertical: 50.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Create Account',
                      style: GoogleFonts.openSans(
                        textStyle: TextStyle(
                          color: Colors.white,
                          fontSize: 32.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    SizedBox(height: 30.0),
                    TextFieldName(),
                    SizedBox(height: 30.0),
                    TextFieldSeEmail(),
                    SizedBox(height: 30.0),
                    TextFieldPAss(),
                    ButtonSignUp(),
                    _textFinal()
                  ],
                ),
              ))
        ],
      ),
    );
  }

  Widget _textFinal() {
    return Column(
      children: [
        Text(
          ' Enjoy with Us ',
          style: TextStyle(
            fontSize: 18.5,
            color: Colors.white,
            fontWeight: FontWeight.w700,
          ),
        ),
        
        
      ],
    );
  }
}
