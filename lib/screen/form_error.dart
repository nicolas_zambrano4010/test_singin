import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class FormError extends StatelessWidget {
  const FormError({Key key, @required this.errors}) : super(key: key);

  final List<String> errors;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        top: 10.0,
        left: 15.0,
      ),
      child: Column(
        
        children: List.generate(
          errors.length,
          (index) => formErrorText(
            error: errors[index],
          ),
        ),
      ),
    );
  }

  Row formErrorText({String error}) {
    return Row(
      children: [
        SvgPicture.asset(
          "assets/icons/Error.svg",
          height: 14,
          width: 14,
          color: Colors.teal[900],
        ),
        SizedBox(
          width: 15,
        ),
        Text(error),
      ],
    );
  }
}
