import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:sing_in_test/screen/FirstPage_Screen/components/Sign_in/sing_form.dart';
import 'package:sing_in_test/screen/FirstPage_Screen/components/otherSingIn/other_Sing_In.dart';
import 'package:sing_in_test/utilities/constans.dart';

class LoginScreen extends StatefulWidget {
  LoginScreen({Key key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

List<Color> colorList = [
  Colors.purple[200],
  Colors.purple[300],
  Colors.purple[400],
  Colors.purple
];

class _LoginScreenState extends State<LoginScreen> {
  bool _rememberMe = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            height: double.infinity,
            width: double.infinity,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: colorList,
                  stops: [0.1, 0.4, 0.7, 0.9]),
            ),
          ),
          Container(
            height: double.infinity,
            child: SingleChildScrollView(
              physics: AlwaysScrollableScrollPhysics(),
              padding: EdgeInsets.symmetric(horizontal: 40.0, vertical: 120.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'Sign In',
                    style: GoogleFonts.openSans(
                      textStyle: TextStyle(
                        color: Colors.white,
                        fontSize: 30.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 30.0,
                  ),
                  SignForm(),
                  _buttinSingIn(),
                  OtherSignIn(),
                  GestureDetector(
                    onTap: () => Navigator.pushNamed(context, '/SecondPage'),
                    child: RichText(
                      text: TextSpan(
                        children: [
                          TextSpan(
                            text: 'Don\'t have Account? ',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 18.0,
                                fontWeight: FontWeight.w400),
                          ),
                          TextSpan(
                            text: 'Sign Up',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 18.0,
                                fontWeight: FontWeight.bold),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

Widget _buttinSingIn() {
  return Column(
    children: [
      Text(
        ' - OR -',
        style: TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.w500,
        ),
      ),
      SizedBox(height: 20.0),
      Text(
        'Sing In With',
        style: kLabelStyle,
      )
    ],
  );
}
