import 'package:flutter/material.dart';


class OtherSignIn extends StatefulWidget {
  OtherSignIn({Key key}) : super(key: key);

  @override
  _OtherSignInState createState() => _OtherSignInState();
}

class _OtherSignInState extends State<OtherSignIn> {
  @override
  Widget build(BuildContext context) {
    return Container(
       child: Padding(
                    padding:  EdgeInsets.symmetric(vertical: 30.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        GestureDetector(
                          onTap: () => print('Login with Facebook'),
                          child: Container(
                            height: 60.0,
                            width: 60.0,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.black26,
                                      offset: Offset(0, 2),
                                      blurRadius: 6.0)
                                ],
                                image: DecorationImage(
                                  image: AssetImage('assets/logos/facebook.jpg'),
                                )),
                          ),
                        ),
                        GestureDetector(
                          onTap: () => print('Login with google'),
                          child: Container(
                            height: 60.0,
                            width: 60.0,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.black26,
                                  offset: Offset(0, 2),
                                  blurRadius: 6.0,
                                ),
                              ],
                              image: DecorationImage(
                                image: AssetImage('assets/logos/google.jpg'),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  )
    );
  }
}