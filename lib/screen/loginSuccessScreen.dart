import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class LoginSuccessScreen extends StatelessWidget {
  static String routeName = "/loginsuccess";

  List<Color> colorList = [
    Colors.purple[200],
    Colors.purple[300],
    Colors.purple[400],
    Colors.purple
  ];
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Scaffold(
      body: Stack(
        children: [
          Container(
            height: double.infinity,
            width: double.infinity,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: colorList,
                  stops: [0.1, 0.4, 0.7, 0.9]),
            ),
          ),
          Column(
            children: [
              SizedBox(height: 100.0),
              Image.asset(
                'assets/images/success.png',
                height: 500,
              ),
              SizedBox(height: 40,),
              Text(
                "Login Success",
                style: TextStyle(
                  fontSize: 40.0,
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                ),
              ),
            ],
          )
        ],
      ),
    ));
  }
}
